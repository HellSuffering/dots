﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBlock : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    private void Start()
    {
        BindButtonCallback();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Called when block ID change.
    /// Update UI elements
    /// </summary>
    private void OnBlockIDChanged()
    {
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        BlockData blockData = blockSettings.GetBlockData(m_BlockID);

        m_BlockImage.sprite = blockData.Sprite;
        m_BlockText.text = m_BlockID.ToString();
    }

    /// <summary>
    /// Select the associated block as selected block for map edition
    /// </summary>
    private void Select()
    {
        MapEditorBrush.Instance.BlockID = m_BlockID;
        m_BlocksWindow.Close();
    }

    /// <summary>
    /// Bind callback to button on click event
    /// </summary>
    private void BindButtonCallback()
    {
        m_Button.onClick.AddListener(Select);
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Set block ID associated to this UI element
    /// </summary>
    public EBlockID BlockID
    {
        get { return m_BlockID; }

        set
        {
            m_BlockID = value;

            OnBlockIDChanged();
        }
    }

    /// <summary>
    /// Blocks window in which this UI element is present
    /// </summary>
    public UIMapEditorBlocksWindow BlocksWindow
    {
        set { m_BlocksWindow = value; }
    }

    #endregion

    #region Private Members

    // UI image used to show block preview
    [Tooltip("UI image used to show block preview")]
    [SerializeField]
    private Image m_BlockImage = null;

    // UI text used to display block name
    [Tooltip("UI text used to display block name")]
    [SerializeField]
    private Text m_BlockText = null;

    // UI button used to select block on click
    [Tooltip("UI button used to select block on click")]
    [SerializeField]
    private Button m_Button = null;

    // Block ID associated to this UI element
    private EBlockID m_BlockID;

    // Blocks window in which this UI element is present
    private UIMapEditorBlocksWindow m_BlocksWindow = null;

    #endregion
}
