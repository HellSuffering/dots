﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindow : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    protected virtual void Start()
    {
        Close();
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Show all UI elements
    /// </summary>
    public void Open()
    {
        m_IsOpen = true;
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Hide all UI elements
    /// </summary>
    public void Close()
    {
        m_IsOpen = false;
        gameObject.SetActive(false);
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Is window open
    /// </summary>
    public bool IsOpen
    {
        get { return m_IsOpen; }
    }

    #endregion

    #region Private Methods

    // Is window open
    private bool m_IsOpen = false;

    #endregion
}
