﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMapEditorBlocksWindow : UIWindow
{
    #region MonoBehaviour

    // Start is called before the first frame update
    protected override void Start()
    {
        FillContent();

        base.Start();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Fill window iterating on blocks settings and creating 
    /// UI elements for each block ID
    /// </summary>
    private void FillContent()
    {
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        BlockData[] blockDataArray = blockSettings.BlockDataArray;
        BlockData blockData;
        UIBlock uiBlock = null;

        for (int i = 0; i < blockDataArray.Length; i++)
        {
            blockData = blockDataArray[i];

            uiBlock = Instantiate(m_UIBlockPrefab, m_ContentChild);
            uiBlock.BlockID = blockData.BlockID;
            uiBlock.BlocksWindow = this;
        }
    }

    #endregion

    #region Private Members

    // Transform of child having all content in children
    [Tooltip("Transform of child having all content in children")]
    [SerializeField]
    private Transform m_ContentChild = null;

    // Prefab to spawn for each block. UI element showing the block data
    [Tooltip("Prefab to spawn for each block. UI element showing the block data")]
    [SerializeField]
    private UIBlock m_UIBlockPrefab = null;

    #endregion
}
