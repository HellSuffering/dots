﻿using UnityEngine;
using UnityEngine.UI;

public class UIMatchTimer : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimerText();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Update timer text from elapsed time.
    /// Display in duration - elapsed format
    /// </summary>
    private void UpdateTimerText()
    {
        int time = (int)(m_MatchManager.MatchDuration - m_MatchManager.ElapsedTime);
        m_TimerUIText.text = time.ToString();
    }

    #endregion

    #region Private Members

    // UI text used to display match timer
    [SerializeField]
    private Text m_TimerUIText = null;

    // Match manager used to get match elapsed time
    [SerializeField]
    private MatchManager m_MatchManager = null;

    #endregion
}
