﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIMapEditorSelectedBlockButton : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    private void Start()
    {
        MapEditorBrush.Instance.OnBlockIDChanged += UpdateButtonImageFromBlockID;
    }

    // Called when mouse enter the UI element
    public void OnPointerEnter()
    {
        m_IsMouseOver = true;   
    }

    // Called when mouse exit the UI element
    public void OnPointerExit()
    {
        m_IsMouseOver = false;
    }

    #endregion

    #region Public Members

    /// <summary>
    /// Called on button clicked.
    /// Open / close blocks window
    /// </summary>
    public void OnClick()
    {
        if (m_BlocksWindow.IsOpen)
            CloseBlocksWindow();
        else
            OpenBlocksWindow();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Open blocks window
    /// </summary>
    private void OpenBlocksWindow()
    {
        m_BlocksWindow.Open();
    }

    /// <summary>
    /// Close blocks window
    /// </summary>
    private void CloseBlocksWindow()
    {
        m_BlocksWindow.Close();
    }

    /// <summary>
    /// Update button image using sprite of block having specified block ID
    /// </summary>
    /// <param name="_BlockID">ID of selected block</param>
    private void UpdateButtonImageFromBlockID(EBlockID _BlockID)
    {
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        BlockData blockData = blockSettings.GetBlockData(_BlockID);
        m_BlockImage.sprite = blockData.Sprite;
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Blocks window
    /// </summary>
    public UIMapEditorBlocksWindow BlocksWindow
    {
        get { return m_BlocksWindow; }
        set { m_BlocksWindow = value; }
    }

    /// <summary>
    /// Is mouse over this UI elements
    /// </summary>
    public bool IsMouseOver
    {
        get { return m_IsMouseOver; }
    }

    #endregion

    #region Private Members

    // UI Image used to show block sprite
    [Tooltip("UI Image used to show block sprite")]
    [SerializeField]
    private Image m_BlockImage = null;

    // Window showing all blocks, allowing user to select a block to place
    private UIMapEditorBlocksWindow m_BlocksWindow = null;

    // Is mouse over this UI element
    private bool m_IsMouseOver = false;

    #endregion
}
