﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMapEditorHUD : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple instances of UIMapEditorHUD found");
            Destroy(this);
            return;
        }

        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_SelectedBlockButton.BlocksWindow = m_BlocksWindow;
    }

    // Called on destroy
    void OnDestroy()
    {
        s_Instance = null;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Check if any UI window is open or mouse is on a UI button
    /// </summary>
    /// <returns>Any UI window open</returns>
    public bool IsUIFocused()
    {
        return m_BlocksWindow.IsOpen || m_SelectedBlockButton.IsMouseOver;
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static UIMapEditorHUD Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    // Button used to select block to place and show selected block
    [Tooltip("Button used to select block to place and show selected block")]
    [SerializeField]
    private UIMapEditorSelectedBlockButton m_SelectedBlockButton = null;

    // Window showing all blocks, allowing user to select a block to place
    [Tooltip("Window showing all blocks, allowing user to select a block to place")]
    [SerializeField]
    private UIMapEditorBlocksWindow m_BlocksWindow = null;

    // Instance of this class
    private static UIMapEditorHUD s_Instance = null;

    #endregion
}
