﻿using UnityEngine;
using UnityEngine.UI;

public class UIScore : MonoBehaviour
{
    #region Public Methods

    /// <summary>
    /// Called when score changed. Update UI
    /// </summary>
    /// <param name="_Score">New score</param>
    public void OnScoreChanged(int _Score)
    {
        m_ScoreUIText.text = _Score.ToString();
    }

    #endregion

    #region Private Members

    // UI text used to display score
    [Tooltip("UI text used to display score")]
    [SerializeField]
    private Text m_ScoreUIText = null;

    #endregion
}
