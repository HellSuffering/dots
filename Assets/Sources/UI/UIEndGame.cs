﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Unity.Entities;

public class UIEndGame : MonoBehaviour
{
    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        DestroyAllEntities();

        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void OnScoreChanged(int _Score)
    {
        m_ScoreText.text = _Score.ToString();
    }

    private void DestroyAllEntities()
    {
        EntityManagement.Instance.DestroyAll();
    }

    [SerializeField]
    private Text m_ScoreText = null;
}
