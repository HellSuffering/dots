﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public struct Projectile : IComponentData
{
    // Movement speed of the projectile in meter/sec
    public float Speed;

    // Radius of block destruction in block unit
    public int BlockDestructionRadius;

    // Entity owning the projectile component
    public Entity Entity;
}

[UpdateBefore(typeof(PhysicBodySystem))]
public class ProjectileSystem : ComponentSystem
{
    /// <summary>
    /// Called each frame
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Projectile _Projectile, ref PhysicBody _PhysicBody, ref Rotation _Rotation) =>
        {
            float3 direction = math.mul(_Rotation.Value, new float3(0, 0, 1));
            _PhysicBody.Velocity = math.normalizesafe(direction) * _Projectile.Speed;
        });   
    }
}
