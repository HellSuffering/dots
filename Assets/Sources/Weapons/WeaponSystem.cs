﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct Weapon : IComponentData
{
    // Is weapon firing
    public bool IsFiring;

    // Time in seconds to wait between 2 shots
    public float FireRate;

    // Character owning the weapon
    public Character Owner;

    // Last time weapon fired
    public float LastFireTime;
}

public class WeaponSystem : ComponentSystem
{
    /// <summary>
    /// Called each frame
    /// </summary>
    protected override void OnUpdate()
    {
        float time = Time.time;

        Entities.ForEach((ref Weapon _Weapon) =>
        {
            // Check weapon is firing
            if (_Weapon.IsFiring)
            {
                // Check fire rate
                if (time > _Weapon.LastFireTime + _Weapon.FireRate)
                {
                    // Spawn projectile
                    SpawnProjectile(_Weapon.Owner.Entity);

                    _Weapon.LastFireTime = time;
                }
            }
        });
    }

    /// <summary>
    /// Spawn a projectile shot by the weapon
    /// </summary>
    /// <param name="_EntityOwner">Entity owning the weapon</param>
    private void SpawnProjectile(Entity _EntityOwner)
    {
        EntityManager entityManager = World.Active.EntityManager;
        ProjectileFactory projectileFactory = GameSettings.Instance.ProjectileFactory;
        float3 position = entityManager.GetComponentData<Translation>(_EntityOwner).Value;
        quaternion rotation = entityManager.GetComponentData<Rotation>(_EntityOwner).Value;
        Entity projectileEntity = projectileFactory.CreateProjectile(position, rotation);
    }
}
