﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

[CreateAssetMenu(fileName = "New Character Factory", menuName = "Game/Character Factory")]
public class CharacterFactory : ScriptableObject
{
    #region Public Methods

    public Entity SpawnAICharacter(float3 _Position)
    {
        return Entity.Null;
    }

    /// <summary>
    /// Create player character entity
    /// </summary>
    /// <param name="_Position"></param>
    /// <returns></returns>
    public Entity SpawnPlayerCharacter(float3 _Position)
    {
        if (!m_CharacterArchetype.Valid)
            CreateCharacterArchetype();

        EntityManager entityManager = World.Active.EntityManager;
        Entity entity = EntityManagement.Instance.CreateEntity(m_CharacterArchetype);

        Character character = new Character { Speed = m_CharacterSettings.CharacterSpeed, Entity = entity };

        entityManager.SetComponentData(entity, new Translation { Value = _Position });
        entityManager.SetComponentData(entity, character);
        entityManager.SetComponentData(entity, new Gravity { GravityY = m_CharacterSettings.GravityY });
        entityManager.SetComponentData(entity, new GridMovement { Position = (int3)_Position, NextPosition = (int3)_Position });
        entityManager.SetComponentData(entity, new Weapon { Owner = character, FireRate = 0.5f });

        entityManager.SetSharedComponentData(entity, new RenderMesh { mesh = m_CharacterSettings.Mesh, material = m_CharacterSettings.PlayerMaterial });

        return entity;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Create entity archetype for character entities
    /// </summary>
    private void CreateCharacterArchetype()
    {
        EntityManager entityManager = World.Active.EntityManager;

        m_CharacterArchetype = entityManager.CreateArchetype(
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(Rotation),
            typeof(PhysicBody),
            typeof(Character),
            typeof(PlayerCharacter),
            typeof(Gravity),
            typeof(GridMovement),
            typeof(Weapon)
            );
    }

    #endregion

    #region Private Members

    /// <summary>
    /// Character settings to use to spawn characters
    /// </summary>
    [SerializeField]
    private CharacterSettings m_CharacterSettings;

    /// <summary>
    /// Entity archetype to use for characters
    /// </summary>
    private EntityArchetype m_CharacterArchetype;

    #endregion
}
