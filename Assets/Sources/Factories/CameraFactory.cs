﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

[CreateAssetMenu(fileName = "New Camera Factory", menuName = "Game/Camera Factory")]
public class CameraFactory : ScriptableObject
{
    #region Public Methods

    /// <summary>
    /// Create player camera entity from entity archetype
    /// </summary>
    /// <returns></returns>
    public Entity CreatePlayerCameraEntity()
    {
        if (!m_EntityArchetype.Valid)
            CreateEntityArchetype();

        EntityManager entityManager = World.Active.EntityManager;
        Entity entity = EntityManagement.Instance.CreateEntity(m_EntityArchetype);

        entityManager.SetComponentData(entity, new PlayerCamera { InterpolationSpeed = m_CameraInterpolationSpeed, Offset = m_CameraOffset });

        return entity;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Create player camera entity archetype
    /// </summary>
    public void CreateEntityArchetype()
    {
        EntityManager entityManager = World.Active.EntityManager;
        m_EntityArchetype = entityManager.CreateArchetype
            (
            typeof(PlayerCamera),
            typeof(Translation),
            typeof(Rotation)
            );
    }

    #endregion

    #region Private Members

    // Interpolation speed of player camera used to follow target
    [Tooltip("Interpolation speed of player camera used to follow target")]
    [SerializeField]
    private float m_CameraInterpolationSpeed = 10;

    // Offset to add to target to follow to place camera (sort of relative position)
    [Tooltip("Offset to add to target to follow to place camera (sort of relative position)")]
    [SerializeField]
    private float3 m_CameraOffset = new float3(0, 25, -15);

    // Entity archetype used to create player camera entity
    private EntityArchetype m_EntityArchetype;

    #endregion
}
