﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;

[CreateAssetMenu(fileName = "New Projectile Factory", menuName = "Game/Projectile Factory")]
public class ProjectileFactory : ScriptableObject
{
    #region Public Methods

    /// <summary>
    /// Create a new projectile entity and set its component data
    /// </summary>
    /// <param name="_Position">Position of the projectile entity</param>
    /// <param name="_Rotation">Rotation of the projectile entity</param>
    /// <returns>Projectile entity</returns>
    public Entity CreateProjectile(float3 _Position, quaternion _Rotation)
    {
        if (!m_EntityArchetype.Valid)
            CreateEntityArchetype();

        EntityManager entityManager = World.Active.EntityManager;
        Entity entity = EntityManagement.Instance.CreateEntity(m_EntityArchetype);

        entityManager.SetComponentData(entity, new Projectile { Speed = m_ProjectileSpeed, Entity = entity, BlockDestructionRadius = m_BlockDestructionRadius });
        entityManager.SetComponentData(entity, new Translation { Value = _Position });
        entityManager.SetComponentData(entity, new Rotation { Value = _Rotation });
        entityManager.SetComponentData(entity, new LifeTime { Value = m_ProjectileLifeTime, CreationTime = Time.time, Entity = entity });
        entityManager.SetSharedComponentData(entity, new RenderMesh { mesh = m_Mesh, material = m_Material });

        return entity;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Create entity archetype used to spawn projectiles
    /// </summary>
    private void CreateEntityArchetype()
    {
        EntityManager entityManager = World.Active.EntityManager;
        m_EntityArchetype = entityManager.CreateArchetype
            (
            typeof(Projectile),
            typeof(Translation),
            typeof(Rotation),
            typeof(PhysicBody),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(LifeTime)
            );
    }

    #endregion

    #region Private Members

    // Mesh used to render projectile
    [Tooltip("Mesh used to render projectile")]
    [SerializeField]
    private Mesh m_Mesh = null;

    // Material used to render projectile
    [Tooltip("Material used to render projectile")]
    [SerializeField]
    private Material m_Material = null;

    // Projectile speed in meter/sec
    [Tooltip("Projectile speed in meter/sec")]
    [SerializeField]
    private float m_ProjectileSpeed = 10;

    // Life time in seconds for spawned projectiles
    [Tooltip("Life time in seconds for spawned projectiles")]
    [SerializeField]
    private float m_ProjectileLifeTime = 5;

    // Radius of block destruction in block unit
    [Tooltip("Radius of block destruction in block unit")]
    [SerializeField]
    private int m_BlockDestructionRadius = 1;

    // Entity archetype used to create projectiles
    private EntityArchetype m_EntityArchetype;

    #endregion
}
