﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public class MapEditorFileSaveLoad : MonoBehaviour
{
    #region Public Methods

    /// <summary>
    /// Save editor map to map file
    /// </summary>
    /// <param name="_MapName">Name of the map file</param>
    public void SaveMap(string _MapName)
    {
        string mapData = GetMapDataString();

        string path = Application.dataPath + "\\" + Globals.MapFolder + "\\";
        System.IO.File.WriteAllText(path + _MapName, mapData);
    }

    /// <summary>
    /// Load map file and fill grid with loaded blocks
    /// </summary>
    /// <param name="_MapName">Name of map file to load</param>
    public void LoadMap(string _MapName)
    {
        string path = Application.dataPath + "\\" + Globals.MapFolder + "\\";
        EBlockID[,,] blockDataArray = MapLoader.LoadBlocks(path + _MapName);
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        EBlockID blockID;
        int3 position;

        if (blockDataArray != null)
        {
            for (int x = 0; x < blockDataArray.GetLength(0); x++)
            {
                for (int y = 0; y < blockDataArray.GetLength(1); y++)
                {
                    for (int z = 0; z < blockDataArray.GetLength(2); z++)
                    {
                        blockID = blockDataArray[x, y, z];
                        position = new int3(x, y, z);
                        //blockSettings.BlockFactory.CreateBlock(blockID, position);
                        m_MapEditorGrid.SetBlockAt(position, blockID);
                    }
                }
            }
        }

        m_MapEditorGrid.RefreshVisibility();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Get map data as string to save to file
    /// </summary>
    /// <returns>Map data as string to save to file</returns>
    private string GetMapDataString()
    {
        Block[,,] blockArray = m_MapEditorGrid.BlockArray;
        string strData = "";

        // Save map size
        for (int i = 0; i < 3; i++)
            strData += m_MapEditorGrid.BlockArray.GetLength(i).ToString() + ";";

        // Save blocks
        for (int x = 0; x < m_MapEditorGrid.BlockArray.GetLength(0); x++)
        {
            for (int y = 0; y < m_MapEditorGrid.BlockArray.GetLength(1); y++)
            {
                for (int z = 0; z < m_MapEditorGrid.BlockArray.GetLength(2); z++)
                {
                    strData += ((int)m_MapEditorGrid.BlockArray[x, y, z].BlockData.BlockID).ToString();
                    strData += ";";
                }
            }
        }

        return strData;
    }

    #endregion

    #region Private Members

    // Map editor grid used to get blocks to save
    [Tooltip("Map editor grid used to get blocks to save")]
    [SerializeField]
    private MapEditorGrid m_MapEditorGrid = null;

    #endregion
}
