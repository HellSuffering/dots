﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEditorCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Public Methods

    /// <summary>
    /// Move camera in specified direction checking grid limits
    /// </summary>
    /// <param name="_Direction">Direction in which to move. Will be normalized</param>
    public void Move(Vector3 _Direction)
    {
        if (_Direction == Vector3.zero)
            return;

        // Move camera
        _Direction = _Direction.normalized * m_Speed;
        transform.position += _Direction * Time.deltaTime;

        // Check bounds on X axis
        if (transform.position.x > m_MapGrid.transform.position.x + m_MapGrid.SizeX)
            transform.position = new Vector3(m_MapGrid.transform.position.x + m_MapGrid.SizeX, transform.position.y, transform.position.z);
        else if (transform.position.x < m_MapGrid.transform.position.x)
            transform.position = new Vector3(0, transform.position.y, transform.position.z);

        // Check bounds on Y axis
        if (transform.position.y < m_MinPosY)
            transform.position = new Vector3(transform.position.x, m_MinPosY, transform.position.z);
        else if (transform.position.y > m_MaxPosY)
            transform.position = new Vector3(transform.position.x, m_MaxPosY, transform.position.z);

        // Check bounds on Z axis
        if (transform.position.z > m_MapGrid.transform.position.z + m_MapGrid.SizeZ)
            transform.position = new Vector3(transform.position.x, transform.position.y, m_MapGrid.transform.position.z + m_MapGrid.SizeZ);
        else if (transform.position.z < m_MapGrid.transform.position.z)
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    #endregion

    #region Private Members

    // Camera speed in meter/sec
    [Tooltip("Camera speed in meter/sec")]
    [SerializeField]
    private float m_Speed = 25;

    // Map grid used for map edition
    [Tooltip("Map grid used for map edition")]
    [SerializeField]
    private MapEditorGrid m_MapGrid = null;

    // Minimum position on up axis for zoom
    [Tooltip("Minimum position on up axis for zoom")]
    [SerializeField]
    private float m_MinPosY = 10;

    // Maximum position on up axis for zoom
    [Tooltip("Maximum position on up axis for zoom")]
    [SerializeField]
    private float m_MaxPosY = 50;

    #endregion
}
