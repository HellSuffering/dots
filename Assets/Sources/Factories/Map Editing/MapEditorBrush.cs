﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public class MapEditorBrush : MonoBehaviour
{
    #region Delegates

    // Used to notify block ID has changed
    public delegate void BlockIDChanged(EBlockID _BlockID);

    #endregion

    #region MonoBehaviour Methods

    // Called at creation$
    private void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple instances of MapEditorBrush found");
            Destroy(this);
            return;
        }

        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        BlockID = EBlockID.STONE; // Default block
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCellPosition();
    }

    // Called on destroy
    private void OnDestroy()
    {
        s_Instance = null;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Place block on selected cell
    /// </summary>
    public void PlaceBlock()
    {
        m_MapGrid.SetBlockAt(m_SelectedCellPosition.x, m_SelectedCellPosition.y, m_BlockID);
    }

    /// <summary>
    /// Remove block on selected cell
    /// </summary>
    public void RemoveBlock()
    {
        m_MapGrid.RemoveBlockAt(m_SelectedCellPosition.x, m_SelectedCellPosition.y);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Update selected cell position ray casting on grid plane
    /// </summary>
    private void UpdateCellPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, m_LayerMask.value))
        {
            m_SelectedCellPosition.x = (int)(hit.point.x);
            m_SelectedCellPosition.y = (int)(hit.point.z);

            m_BrushTransform.position = new Vector3((int)hit.point.x, (int)hit.point.y, (int)hit.point.z);
        }
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Block ID accessors
    /// </summary>
    public EBlockID BlockID
    {
        get { return m_BlockID; }

        set
        {
            m_BlockID = value;

            m_OnBlockIDChanged.Invoke(m_BlockID);
        }
    }

    /// <summary>
    /// Invoked on block ID changed
    /// </summary>
    public BlockIDChanged OnBlockIDChanged
    {
        get { return m_OnBlockIDChanged; }
        set { m_OnBlockIDChanged = value; }
    }

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static MapEditorBrush Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    // Layers on which to perform ray cast to select cell
    [Tooltip("Layers on which to perform ray cast to select cell")]
    [SerializeField]
    private LayerMask m_LayerMask;

    // Map grid used for map edition
    [Tooltip("Map grid used for map edition")]
    [SerializeField]
    private MapEditorGrid m_MapGrid = null;

    // Transform of game object used to represent brush in scene
    [Tooltip("Transform of game object used to represent brush in scene")]
    [SerializeField]
    private Transform m_BrushTransform = null;

    // ID of block to place on map
    private EBlockID m_BlockID;

    // 2D position of selected cell
    private int2 m_SelectedCellPosition;

    // Invoked when block ID change
    private BlockIDChanged m_OnBlockIDChanged;

    // Instance of this class
    private static MapEditorBrush s_Instance = null;

    #endregion
}
