﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEditorController : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckInputs();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Check inputs for map edition
    /// </summary>
    private void CheckInputs()
    {
        // Check we are not navigating in UI
        if (UIMapEditorHUD.Instance.IsUIFocused())
            return;

        // Floor inputs
        if (Input.GetKeyDown(KeyCode.UpArrow))
            m_MapGrid.NextFloor();
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            m_MapGrid.PreviousFloor();

        // Camera inputs
        Vector3 cameraMovement = Vector3.zero;

        if (Input.GetKey(KeyCode.Z))
            cameraMovement.z = 1;
        else if (Input.GetKey(KeyCode.S))
            cameraMovement.z = -1;

        if (Input.GetKey(KeyCode.Q))
            cameraMovement.x = -1;
        else if (Input.GetKey(KeyCode.D))
            cameraMovement.x = 1;

        if (Input.mouseScrollDelta.y > 0)
            cameraMovement.y = 1;
        else if (Input.mouseScrollDelta.y < 0)
            cameraMovement.y = -1;

        m_Camera.Move(cameraMovement);

        // Brush input
        if (Input.GetMouseButton(0))
            m_Brush.PlaceBlock();
        else if (Input.GetMouseButton(1))
            m_Brush.RemoveBlock();
    }

    #endregion

    #region Private Members

    // Map grid used to edit map
    [Tooltip("Map grid used to edit map")]
    [SerializeField]
    private MapEditorGrid m_MapGrid = null;

    // Camera to use for map editor view
    [Tooltip("Camera to use for map editor view")]
    [SerializeField]
    private MapEditorCamera m_Camera = null;

    // Brush used to place blocks
    [Tooltip("Brush used to place blocks")]
    [SerializeField]
    private MapEditorBrush m_Brush = null;

    #endregion
}
