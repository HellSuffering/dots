﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public class MapEditorGrid : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Start is called before the first frame update
    void Start()
    {
        InitializeGrid();
    }

    // Draw gizmos
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawWireCube(new Vector3(m_GridX * 0.5f, 0, m_GridZ * 0.5f), new Vector3(m_GridX, 0.01f, m_GridZ));
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Set grid to next floor and update visibility
    /// </summary>
    public void NextFloor()
    {
        if (m_CurrentFloor + 1 >= m_GridY)
            return;

        m_CurrentFloor++;
        ShowCurrentFloor();
        transform.position += Vector3.up;
    }

    /// <summary>
    /// Set grid to previous floor and update visibility
    /// </summary>
    public void PreviousFloor()
    {
        if (m_CurrentFloor <= 0)
            return;

        m_CurrentFloor--;
        HideUpperFloors();
        transform.position -= Vector3.up;
    }

    /// <summary>
    /// Place specified block at given position 
    /// (Y not specified as current floor will be used)
    /// </summary>
    /// <param name="_X">Position on X axis</param>
    /// <param name="_Z">Position on Z axis</param>
    /// <param name="_BlockID">IF of block to place</param>
    public void SetBlockAt(int _X, int _Z, EBlockID _BlockID)
    {
        SetBlockAt(new int3(_X, m_CurrentFloor, _Z), _BlockID);
    }

    /// <summary>
    /// Place specified block at given position
    /// </summary>
    /// <param name="_Position">Position in grid</param>
    /// <param name="_BlockID">ID of block to create</param>
    public void SetBlockAt(int3 _Position, EBlockID _BlockID)
    {
        if (!IsValidPosition(_Position))
            return;

        EntityManager entityManager = World.Active.EntityManager;
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        Block block = m_BlockArray[_Position.x, _Position.y, _Position.z];

        if (block.Entity != Entity.Null)
            EntityManagement.Instance.DestroyEntity(block.Entity);

        block = blockSettings.BlockFactory.CreateBlock(_BlockID, new float3(_Position.x + 0.5f, _Position.y, _Position.z + 0.5f));
        m_BlockArray[_Position.x, _Position.y, _Position.z] = block;
    }

    /// <summary>
    /// Remove block at specified position on grid
    /// Position on up axis not specified as we will use the current floor
    /// </summary>
    /// <param name="_X">Position on X axis</param>
    /// <param name="_Z">Position on Z axis</param>
    public void RemoveBlockAt(int _X, int _Z)
    {
        if (_X >= 0 && _X < m_GridX && _Z >= 0 && _Z < m_GridZ)
        {
            EntityManager entityManager = World.Active.EntityManager;
            Block block = m_BlockArray[_X, m_CurrentFloor, _Z];

            if (block.Entity != Entity.Null)
                EntityManagement.Instance.DestroyEntity(block.Entity);

            m_BlockArray[_X, m_CurrentFloor, _Z] = new Block();
        }
    }

    /// <summary>
    /// Hide upper floors, should be called on map load to ensure all
    /// loaded blocks have the right visibility
    /// </summary>
    public void RefreshVisibility()
    {
        HideUpperFloors();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Update transform according to grid size
    /// </summary>
    private void InitializeGrid()
    {
        // Initialize block array
        m_BlockArray = new Block[m_GridX, m_GridY, m_GridZ];

        // Get child
        Transform child = transform.GetChild(0);

        // Grid rendering
        child.transform.localScale = new Vector3(m_GridX, m_GridZ, 1);
        child.transform.position = new Vector3(m_GridX * 0.5f, 0, m_GridZ * 0.5f);

        Renderer renderer = child.GetComponent<Renderer>();

        // Note that this doesn't work with Lightweight Render Pipeline shaders, 
        // should use classic unlit shaders
        renderer.material.mainTextureScale = new Vector2(m_GridX, m_GridZ);
    }

    /// <summary>
    /// Show all blocks at current floor
    /// </summary>
    private void ShowCurrentFloor()
    {
        EntityManager entityManager = World.Active.EntityManager;
        Entity entity;

        for (int x = 0; x < m_GridX; x++)
        {
            for (int z = 0; z < m_GridZ; z++)
            {
                entity = m_BlockArray[x, m_CurrentFloor, z].Entity;

                if (entity != Entity.Null)
                    entityManager.SetEnabled(entity, true);
            }
        }
    }

    /// <summary>
    /// Hide all blocks upper than current floor
    /// </summary>
    private void HideUpperFloors()
    {
        EntityManager entityManager = World.Active.EntityManager;
        Entity entity;

        for (int x = 0; x < m_GridX; x++)
        {
            for (int y = m_CurrentFloor + 1; y < m_GridY; y++)
            {
                for (int z = 0; z < m_GridZ; z++)
                {
                    entity = m_BlockArray[x, y, z].Entity;

                    if (entity != Entity.Null)
                        entityManager.SetEnabled(entity, false);
                }
            }
        }
    }

    /// <summary>
    /// Check if specified position in the grid is valid (index not out of bounds)
    /// </summary>
    /// <param name="_Position">3D index to check</param>
    /// <returns>True if value, false if out of bounds</returns>
    private bool IsValidPosition(int3 _Position)
    {
        if (_Position.x < 0 || _Position.x >= m_BlockArray.GetLength(0))
            return false;
        else if (_Position.y < 0 || _Position.y >= m_BlockArray.GetLength(1))
            return false;
        else if (_Position.z < 0 || _Position.z >= m_BlockArray.GetLength(2))
            return false;

        return true;
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Get grid size on X axis
    /// </summary>
    public int SizeX
    {
        get { return m_GridX; }
    }

    /// <summary>
    /// Get grid size on Y axis
    /// </summary>
    public int SizeY
    {
        get { return m_GridY; }
    }

    /// <summary>
    /// Get grid size on Z axis
    /// </summary>
    public int SizeZ
    {
        get { return m_GridZ; }
    }

    /// <summary>
    /// Get block array
    /// </summary>
    public Block[,,] BlockArray
    {
        get { return m_BlockArray; }
    }

    #endregion

    #region Private Members

    // Size of grid in block on X axis
    [Tooltip("Size of grid in block on X axis")]
    [SerializeField]
    private int m_GridX = 50;

    // Size of grid in block on Y axis
    [Tooltip("Size of grid in block on Y axis")]
    [SerializeField]
    private int m_GridY = 4;

    // Size of grid in block on Z axis
    [Tooltip("Size of grid in block on Z axis")]
    [SerializeField]
    private int m_GridZ = 50;

    // Mesh used to draw grid
    [Tooltip("Mesh used to draw grid")]
    [SerializeField]
    private Mesh m_Mesh = null;

    // Index of floor to edit
    private int m_CurrentFloor = 0;

    // Array of all blocks of the map
    private Block[,,] m_BlockArray = null;

    // Game object rendering the grid mesh
    private GameObject m_GridGameObject = null;

    #endregion
}
