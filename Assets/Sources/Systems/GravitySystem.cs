﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct Gravity : IComponentData
{
    /// <summary>
    /// Gravity in m/sec on world up axis
    /// </summary>
    public float GravityY;
}

//[UpdateAfter(typeof(CollisionSystem))]
[UpdateBefore(typeof(PhysicBodySystem))]
public class GravitySystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.deltaTime;

        Entities.ForEach((ref Gravity _Gravity, ref PhysicBody _PhysicBody) =>
        {
            _PhysicBody.Velocity.y += _Gravity.GravityY;

            if (math.abs(_PhysicBody.Velocity.y) > math.abs(_Gravity.GravityY))
            {
                _PhysicBody.Velocity.y = _Gravity.GravityY;
            }
        });
    }
}