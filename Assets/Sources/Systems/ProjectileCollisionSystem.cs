﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateBefore(typeof(PhysicBodySystem))]
public class ProjectileCollisionSystem : ComponentSystem
{
    /// <summary>
    /// Called each frame
    /// </summary>
    protected override void OnUpdate()
    {
        float deltaTime = Time.deltaTime;

        Entities.ForEach((ref Projectile _Projectile, ref Translation _Translation, ref PhysicBody _PhysicBody) =>
        {
            float3 position = _Translation.Value;
            bool isColliding = CheckGridCollision(position);

            // Ensure projectile is not missing a block in a frame due to high speed
            if (!isColliding)
            {
                int3 gridMovement = (int3)(_PhysicBody.Velocity * deltaTime);

                if (math.lengthsq(gridMovement) > 1)
                {
                    position += math.normalizesafe(gridMovement);
                    isColliding = CheckGridCollision(position);
                }
            }

            if (isColliding)
            {
                // Destroy hit block
                RemoveBlockAt((int3)position, _Projectile.BlockDestructionRadius);

                // Destroy projectile
                EntityManagement.Instance.DestroyEntity(_Projectile.Entity);
            }
        });
    }

    /// <summary>
    /// Check collision on grid
    /// </summary>
    /// <param name="_Position">Position where we need collision check</param>
    /// <returns>True on collision, false otherwise</returns>
    private bool CheckGridCollision(float3 _Position)
    {
        if (BlockGrid.Instance.GetBlockAtWorldPosition(_Position).BlockData.BlockID != EBlockID.NONE)
            return true;

        return false;
    }

    /// <summary>
    /// Remove all block in specified radius at given position
    /// </summary>
    /// <param name="_Position">Epicenter</param>
    /// <param name="_Radius">Radius of damage in meter</param>
    private void RemoveBlockAt(int3 _Position, int _Radius)
    {
        for (int x = - _Radius; x <= _Radius; x++)
        {
            for (int y = - _Radius; y <= _Radius; y++)
            {
                for (int z = -_Radius; z <= _Radius; z++)
                {
                    BlockGrid.Instance.RemoveBlockAt(_Position.x + x, _Position.y + y, _Position.z + z);
                }
            }
        }
    }
}
