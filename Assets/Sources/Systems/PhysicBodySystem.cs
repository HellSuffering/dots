﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct PhysicBody : IComponentData
{
    public float3 Velocity;
}

public class PhysicBodySystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.deltaTime;

        Entities.ForEach((ref PhysicBody _PhysicBody, ref Translation _Translation) =>
        {
            _Translation.Value += _PhysicBody.Velocity * deltaTime;
        });
    }
}
