﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Mathematics;

public struct Character : IComponentData
{
    /// <summary>
    /// Character speed in meter/sec
    /// </summary>
    public float Speed;

    /// <summary>
    /// Entity owning this component
    /// </summary>
    public Entity Entity;
}

public struct PlayerCharacter : IComponentData
{

}

[UpdateBefore(typeof(PhysicBodySystem))]
[UpdateAfter(typeof(GridMovementSystem))]
public class CharacterSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Character _Character, ref PhysicBody _PhysicBody, ref Rotation _Rotation) =>
        {
            float3 velocity = _PhysicBody.Velocity;

            // Ensure velocity is not higher than character speed
            if (math.length(velocity) > _Character.Speed)
            {
                velocity = math.normalizesafe(velocity) * _Character.Speed;
                _PhysicBody.Velocity = velocity;
            }

            // Look at in movement direction
            if (velocity.x != 0 || velocity.z != 0)
                _Rotation.Value = quaternion.LookRotation(math.normalizesafe(velocity), new float3(0, 1, 0));
        });
    }
}
