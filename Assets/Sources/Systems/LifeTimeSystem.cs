﻿using UnityEngine;
using Unity.Entities;

public struct LifeTime : IComponentData
{
    /// <summary>
    /// Number of seconds to wait since entity creation before destroying it
    /// </summary>
    public float Value;

    /// <summary>
    /// Time at which entity owning this component has been created
    /// </summary>
    public float CreationTime;

    /// <summary>
    /// Entity owning this component
    /// </summary>
    public Entity Entity;
}

public class LifeTimeSystem : ComponentSystem
{
    /// <summary>
    /// Called each frame
    /// </summary>
    protected override void OnUpdate()
    {
        float time = Time.time;

        Entities.ForEach((ref LifeTime _LifeTime) =>
        {
            if (time > _LifeTime.CreationTime + _LifeTime.Value)
            {
                EntityManagement.Instance.DestroyEntity(_LifeTime.Entity);
            }
        });
    }
}
