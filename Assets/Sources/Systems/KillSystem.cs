﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class KillSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref PlayerCharacter _PlayerCharacter, ref Translation _Translation) =>
        {
            if (_Translation.Value.y < 0)
                MatchManager.Instance.StopMatch();
        });
    }
}
