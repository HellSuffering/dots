﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct GridMovement : IComponentData
{
    public int3 Position;
    public int3 NextPosition;
}

[UpdateBefore(typeof(PhysicBodySystem))]
[UpdateAfter(typeof(GravitySystem))]
public class GridMovementSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.deltaTime;

        Entities.ForEach((ref GridMovement _GridMovement, ref PhysicBody _PhysicBody, ref Character _Character, ref Translation _Translation) =>
        {
            float3 velocity = float3.zero;

            // Ensure we are not moving in a block
            Block nextBlock = BlockGrid.Instance.GetBlockAtWorldPosition(_GridMovement.NextPosition);

            if (nextBlock.BlockData.BlockID == EBlockID.NONE)
            {
                // Get movement velocity
                float3 movement = _GridMovement.NextPosition - _Translation.Value;
                movement.y = 0;
                velocity = movement;
                velocity = math.normalizesafe(velocity) * _Character.Speed;
                velocity.y = _PhysicBody.Velocity.y;

                // Ensure gravity or jump doesn't push the entity in a block
                float movementY = _PhysicBody.Velocity.y * deltaTime;
                float3 nextWorldPosition = _Translation.Value + new float3(0, movementY, 0);
                nextBlock = BlockGrid.Instance.GetBlockAtWorldPosition(nextWorldPosition);

                if (nextBlock.BlockData.BlockID != EBlockID.NONE)
                {
                    velocity.y = 0;
                    _Translation.Value.y = (int)_Translation.Value.y;
                }

                // Check destination reached
                float2 velocity2d = new float2(velocity.x, velocity.z);

                if (math.lengthsq(velocity * deltaTime) > math.lengthsq(movement))
                {
                    // Ensure we don't exceed the destination
                    velocity.x = 0;
                    velocity.z = 0;
                    _Translation.Value.x = _GridMovement.NextPosition.x;
                    _Translation.Value.z = _GridMovement.NextPosition.z;

                    // Update position once we are arrived
                    _GridMovement.Position = new int3(_Translation.Value);
                }
            }


            // Update velocity
            _PhysicBody.Velocity = velocity;
        });
    }
}
