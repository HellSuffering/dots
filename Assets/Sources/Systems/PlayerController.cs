﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public class PlayerControllerSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if (MatchManager.Instance && !MatchManager.Instance.MatchStarted)
            return;

        float3 movement = GetMovementInput();

        Entities.ForEach((
            ref PlayerCharacter _PlayerCharacter, 
            ref Character _Character, 
            ref PhysicBody _PhysicBody, 
            ref GridMovement _GridMovement,
            ref Weapon _Weapon
            ) =>
        {
            if (movement.x != 0 || movement.y != 0 || movement.z != 0)
            {
                if (_PhysicBody.Velocity.x == 0 && _PhysicBody.Velocity.z == 0)
                    _GridMovement.NextPosition = new int3(_GridMovement.Position + movement);
            }

            _Weapon.IsFiring = GetFireInput();
        });
    }

    /// <summary>
    /// Check player inputs and get a movement vector from it
    /// </summary>
    /// <returns>Normalized movement vector from input</returns>
    private float3 GetMovementInput()
    {
        float3 movement = new float3(0, 0, 0);

        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
            movement.z = 1;
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            movement.z = -1;
        else if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
            movement.x = -1;
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            movement.x = 1;

        return movement;
    }

    /// <summary>
    /// Get fire input value
    /// </summary>
    /// <returns>True if firing, false otherwise</returns>
    private bool GetFireInput()
    {
        return Input.GetMouseButton(0);
    }
}