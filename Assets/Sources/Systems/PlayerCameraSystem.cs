﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public struct PlayerCamera : IComponentData
{
    // Offset to add to position to follow
    public float3 Offset;

    // Speed of interpolation used to follow player character smoothly
    public float InterpolationSpeed;
}

public class PlayerCameraSystem : ComponentSystem
{
    #region Component System Methods

    /// <summary>
    /// Called each frame
    /// </summary>
    protected override void OnUpdate()
    {
        // Get target position
        int characterCount = 0;
        float3 averagePosition = float3.zero;

        Entities.ForEach((ref PlayerCharacter _PlayerCharacter, ref Translation _Translation) =>
        {
            averagePosition += _Translation.Value;
            characterCount++;   
        });

        averagePosition /= characterCount;

        // Move camera
        float3 desiredPosition = averagePosition;
        float deltaTime = Time.deltaTime;

        Entities.ForEach((ref PlayerCamera _PlayerCamera, ref Translation _Translation, ref Rotation _Rotation) =>
        {
            // Position
            desiredPosition += _PlayerCamera.Offset;
            _Translation.Value = math.lerp(_Translation.Value, desiredPosition, _PlayerCamera.InterpolationSpeed * deltaTime);

            // Rotation
            float3 forward = -math.normalizesafe(_PlayerCamera.Offset);
            _Rotation.Value = quaternion.LookRotation(forward, new float3(0, 1, 0));

            // Update camera game object
            Camera.main.transform.position = _Translation.Value;
            Camera.main.transform.rotation = _Rotation.Value;
        });
    }

    #endregion
}
