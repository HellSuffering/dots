﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple GameSettings instances found");
            Destroy(this);
            return;
        }

        s_Instance = this;
    }

    // Start is called before the first frame update
    private void Start()
    {
        CreatePlayerEntity();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Create player entity
    /// </summary>
    private void CreatePlayerEntity()
    {
        m_CharacterFactory.SpawnPlayerCharacter(m_PlayerStart.position);
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Block settings to use
    /// </summary>
    public BlockSettings BlockSettings
    {
        get { return m_BlockSettings; }
    }

    /// <summary>
    /// Character factory used to spawn characters
    /// </summary>
    public CharacterFactory CharacterFactory
    {
        get { return m_CharacterFactory; }
    }

    /// <summary>
    /// Player start transform
    /// </summary>
    public Transform PlayerStart
    {
        get { return m_PlayerStart; }
    }

    /// <summary>
    /// Get projectile factory
    /// </summary>
    public ProjectileFactory ProjectileFactory
    {
        get { return m_ProjectileFactory; }
    }

    /// <summary>
    /// Get camera factory
    /// </summary>
    public CameraFactory CameraFactory
    {
        get { return m_CameraFactory; }
    }

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static GameSettings Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    // Character factory to use to spawn characters
    [SerializeField]
    [Tooltip("Character factory to use to spawn characters")]
    private CharacterFactory m_CharacterFactory = null;

    // Block settings to use for the block grid
    [SerializeField]
    [Tooltip("Block settings to use for the block grid")]
    private BlockSettings m_BlockSettings = null;

    // Projectile factory used to spawn projectiles
    [SerializeField]
    [Tooltip("Projectile factory used to spawn projectiles")]
    private ProjectileFactory m_ProjectileFactory = null;

    // Camera factory used to create camera entity
    [SerializeField]
    [Tooltip("Camera factory used to create camera entity")]
    private CameraFactory m_CameraFactory = null;

    // Transform used to define the player start position
    [SerializeField]
    [Tooltip("Transform used to define the player start position")]
    private Transform m_PlayerStart = null;

    // Instance of this class
    private static GameSettings s_Instance;

    #endregion
}
