﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Character Settings", menuName = "Game/Character Settings")]
public class CharacterSettings : ScriptableObject
{
    #region Getters & Setters

    /// <summary>
    /// Characters mesh
    /// </summary>
    public Mesh Mesh
    {
        get { return m_Mesh; }
    }

    /// <summary>
    /// Material to use for player character
    /// </summary>
    public Material PlayerMaterial
    {
        get { return m_PlayerMaterial; }
    }

    /// <summary>
    /// Get character speed
    /// </summary>
    public float CharacterSpeed
    {
        get { return m_CharacterSpeed; }
    }

    /// <summary>
    /// Get gravity in meter/sec on the world up axis
    /// </summary>
    public float GravityY
    {
        get { return m_GravityY; }
    }

    #endregion

    #region Private Members

    // Mesh to use for all characters
    [Tooltip("Mesh to use for all characters")]
    [SerializeField]
    private Mesh m_Mesh;

    // Material to use for player character
    [Tooltip("Material to use for player character")]
    [SerializeField]
    private Material m_PlayerMaterial;

    // Movement speed for all characters in meter/sec
    [Tooltip("Movement speed for all characters in meter/sec")]
    [SerializeField]
    private float m_CharacterSpeed = 10;

    // Gravity in meter/sec to apply on world up axis on all characters
    [Tooltip("Gravity in meter/sec to apply on world up axis on all characters")]
    [SerializeField]
    private float m_GravityY = -9.8f;

    #endregion
}
