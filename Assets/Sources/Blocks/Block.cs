﻿using UnityEngine;
using Unity.Entities;

public enum EBlockID
{
    NONE,
    BEDROCK,
    STONE,
    GRASS,
    BRICK
}

[System.Serializable]
public struct BlockData
{
    public EBlockID BlockID;
    public Material Material;
    public Sprite Sprite;
}

public struct Block
{
    public BlockData BlockData;
    public Entity Entity;
}