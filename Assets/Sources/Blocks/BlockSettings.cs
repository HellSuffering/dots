﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Block Settings", menuName = "Game/Block Settings")]
public class BlockSettings : ScriptableObject
{
    #region Public Methods

    /// <summary>
    /// Get block data of specified ID
    /// </summary>
    /// <param name="_BlockID">ID of black data to return</param>
    /// <returns>Block data found, new empty block data if not found</returns>
    public BlockData GetBlockData(EBlockID _BlockID)
    {
        BlockData blockData;

        for (int i = 0; i < m_BlockDataArray.Length; i++)
        {
            blockData = m_BlockDataArray[i];

            if (blockData.BlockID == _BlockID)
                return blockData;
        }

        return new BlockData();
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Mesh to use for all blocks
    /// </summary>
    public Mesh BlockMesh
    {
        get { return m_BlockMesh; }
    }

    /// <summary>
    /// Block factory used to create blocks
    /// </summary>
    public BlockFactory BlockFactory
    {
        get { return m_BlockFactory; }
    }

    /// <summary>
    /// Get array of all block data
    /// </summary>
    public BlockData[] BlockDataArray
    {
        get { return m_BlockDataArray; }
    }

    #endregion

    #region Private Members

    /// <summary>
    /// Array of all block data
    /// </summary>
    [SerializeField]
    private BlockData[] m_BlockDataArray;

    /// <summary>
    /// Mesh to use for all blocks
    /// </summary>
    [SerializeField]
    private Mesh m_BlockMesh;

    /// <summary>
    /// Block factory used to create blocks
    /// </summary>
    [SerializeField]
    private BlockFactory m_BlockFactory;

    #endregion
}