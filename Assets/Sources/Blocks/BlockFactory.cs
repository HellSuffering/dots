﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;

[CreateAssetMenu(fileName = "New Block Factory", menuName = "Game/Block Factory")]
public class BlockFactory : ScriptableObject
{
    #region Public Methods

    /// <summary>
    /// Create block entity from specified ID at specified position
    /// </summary>
    /// <param name="_BlockID">ID of block to create, used to get right block data</param>
    /// <param name="_Position">Position of the block to create</param>
    /// <returns></returns>
    public Block CreateBlock(EBlockID _BlockID, float3 _Position)
    {
        if (_BlockID == EBlockID.NONE)
            return new Block();

        EntityManager entityManager = World.Active.EntityManager;
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;

        Block block = new Block();
        BlockData blockData = blockSettings.GetBlockData(_BlockID);
        block.BlockData = blockData;
        block.Entity = EntityManagement.Instance.CreateEntity(EntityArchetype);

        entityManager.SetComponentData(block.Entity, new Translation { Value = _Position });
        entityManager.SetSharedComponentData(block.Entity, new RenderMesh { mesh = blockSettings.BlockMesh, material = blockData.Material});

        return block;
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Entity archetype to use for blocks
    /// </summary>
    public EntityArchetype EntityArchetype
    {
        get
        {
            if (!m_EntityArchetype.Valid)
            {
                m_EntityArchetype = World.Active.EntityManager.CreateArchetype(
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation)
                );
            }

            return m_EntityArchetype;
        }
    }

    #endregion

    #region Private Members

    // Entity archetype used to create all block entities
    private EntityArchetype m_EntityArchetype;

    #endregion
}
