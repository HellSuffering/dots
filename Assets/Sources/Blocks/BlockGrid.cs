﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Events;

public class BlockGrid : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    private void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple instances of BlockGrid found");
            Destroy(this);
            return;
        }

        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Create block array and all entities disabled by default
    /// </summary>
    /// <param name="_Nx">X dimension length of block array</param>
    /// <param name="_Ny">Y dimension length of block array</param>
    /// <param name="_Nz">Z dimension length of block array</param>
    public void InitializeBlockGrid(int _Nx, int _Ny, int _Nz)
    {
        //NativeArray<Entity> entityArray = new NativeArray<Entity>(_Nx * _Ny * _Nz, Allocator.Temp);
        m_BlockArray = new Block[_Nx, _Ny, _Nz];
        /*
        EntityManager entityManager = World.Active.EntityManager;
        EntityArchetype entityArchetype = m_BlockSettings.EntityArchetype;
        entityManager.CreateEntity(entityArchetype, entityArray);

        Entity entity;
        int index = 0;

        for (int x = 0; x < _Nx; x++)
        {
            for (int y = 0; y < _Ny; y++)
            {
                for (int z = 0; z < _Nz; z++)
                {
                    entity = entityArray[index];
                    m_BlockArray[x, y, z].Entity = entity;

                    entityManager.SetComponentData(entity, new Translation { Value = new float3(x, y, z) });
                    entityManager.SetSharedComponentData(entity, new RenderMesh { mesh = null, material = null });

                    entityManager.SetEnabled(entity, false);

                    index++;
                }
            }
        }

        entityArray.Dispose();
        */
    }

    /// <summary>
    /// Update block at given 3D index using properties of 
    /// specified block ID in block settings
    /// </summary>
    /// <param name="_X">X index</param>
    /// <param name="_Y">Y index</param>
    /// <param name="_Z">Z index</param>
    /// <param name="_BlockID">ID of the block</param>
    public void SetBlockAt(int _X, int _Y, int _Z, EBlockID _BlockID)
    {
        // Ensure index is not out of bounds
        if (!IsBlockIndexValid(_X, _Y, _Z))
            return;

        // Get block settings
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;

        // Get block properties
        BlockData blockData = blockSettings.GetBlockData(_BlockID);
        EntityManager entityManager = World.Active.EntityManager;
        Block block = m_BlockArray[_X, _Y, _Z];

        // Update block
        if (_BlockID != EBlockID.NONE)
        {
            if (block.Entity != Entity.Null)
                EntityManagement.Instance.DestroyEntity(block.Entity);

            BlockFactory blockFactory = GameSettings.Instance.BlockSettings.BlockFactory;
            block = blockFactory.CreateBlock(_BlockID, new float3(_X, _Y, _Z));
        }
        else
        {
            if (block.Entity != Entity.Null)
            {
                EntityManagement.Instance.DestroyEntity(block.Entity);

                if (m_OnBlockDestroyed != null)
                    m_OnBlockDestroyed.Invoke();
            }

            block = new Block();
        }

        m_BlockArray[_X, _Y, _Z] = block;
    }

    /// <summary>
    /// Set block at specified position to none and disable the entity
    /// </summary>
    /// <param name="_X">X index</param>
    /// <param name="_Y">Y index</param>
    /// <param name="_Z">Z index</param>
    public void RemoveBlockAt(int _X, int _Y, int _Z)
    {
        SetBlockAt(_X, _Y, _Z, EBlockID.NONE);
    }

    /// <summary>
    /// Convert the specified world position to grid position and return 
    /// the the block at this grid position
    /// </summary>
    /// <param name="_WorldPosition">World position of the point to check</param>
    /// <returns>Block at the specified world position</returns>
    public Block GetBlockAtWorldPosition(float3 _WorldPosition)
    {
        int3 gridPosition = WorldPositionToGridPosition(_WorldPosition);

        if (!IsBlockIndexValid(gridPosition))
            return new Block();

        return m_BlockArray[gridPosition.x, gridPosition.y, gridPosition.z];
    }

    /// <summary>
    /// Convert specified world position to grid position
    /// </summary>
    /// <param name="_WorldPosition">World position to convert</param>
    /// <returns>Grid position</returns>
    public int3 WorldPositionToGridPosition(float3 _WorldPosition)
    {
        int3 gridPosition = new int3(_WorldPosition);

        return gridPosition;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Check if 3D index is not out of block array bounds
    /// </summary>
    /// <param name="_X">Index on X dimension</param>
    /// <param name="_Y">Index on Y dimension</param>
    /// <param name="_Z">Index of Z dimension</param>
    /// <returns>Is 3D index valid or not</returns>
    private bool IsBlockIndexValid(int _X, int _Y, int _Z)
    {
        if (_X < 0 || _X >= m_BlockArray.GetLength(0))
            return false;
        else if (_Y < 0 || _Y >= m_BlockArray.GetLength(1))
            return false;
        else if (_Z < 0 || _Z >= m_BlockArray.GetLength(2))
            return false;

        return true;
    }

    /// <summary>
    /// Check if 3D index is not out of block array bounds
    /// </summary>
    /// <param name="_Index3D"></param>
    /// <returns></returns>
    private bool IsBlockIndexValid(int3 _Index3D)
    {
        return IsBlockIndexValid(_Index3D.x, _Index3D.y, _Index3D.z);
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Invoked on block destroyed
    /// </summary>
    public UnityEvent OnBlockDestroyed
    {
        get { return m_OnBlockDestroyed; }
    }

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static BlockGrid Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    /// <summary>
    /// Array of all blocks accessed by 3D position index
    /// </summary>
    private Block[,,] m_BlockArray;

    /// <summary>
    /// Invoked on block destroyed
    /// </summary>
    [SerializeField]
    private UnityEvent m_OnBlockDestroyed;

    /// <summary>
    /// Instance of this class
    /// </summary>
    private static BlockGrid s_Instance;

    #endregion
}
