﻿using Unity.Entities;
using Unity.Collections;
using UnityEngine;

public class EntityManagement : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    private void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple instances of entity management found");
            Destroy(this);
            return;
        }

        s_Instance = this;

        m_EntityList = new NativeList<Entity>(Allocator.Persistent);
    }

    // Called on destroy
    private void OnDestroy()
    {
        m_EntityList.Dispose();
        s_Instance = null;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Create an entity from an entity archetype and add it to list
    /// </summary>
    /// <param name="_Archetype">Archetype of the entity to create</param>
    /// <returns>Created entity</returns>
    public Entity CreateEntity(EntityArchetype _Archetype)
    {
        EntityManager entityManager = World.Active.EntityManager;
        Entity entity = entityManager.CreateEntity(_Archetype);
        m_EntityList.Add(entity);

        return entity;
    }

    /// <summary>
    /// Remove specified entity from list and destroy it
    /// </summary>
    /// <param name="_Entity">Entity to destroy</param>
    public void DestroyEntity(Entity _Entity)
    {
        m_EntityList.RemoveAtSwapBack(m_EntityList.IndexOf(_Entity));
        EntityManager entityManager = World.Active.EntityManager;
        entityManager.DestroyEntity(_Entity);
    }

    /// <summary>
    /// Destroy all created entities and clear list
    /// </summary>
    public void DestroyAll()
    {
        EntityManager entityManager = World.Active.EntityManager;

        for (int i = 0; i < m_EntityList.Length; i++)
        {
            entityManager.DestroyEntity(m_EntityList[i]);
        }

        m_EntityList.Clear();
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static EntityManagement Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    // All created entities
    private NativeList<Entity> m_EntityList;

    // Instance of this class
    private static EntityManagement s_Instance = null;

    #endregion
}
