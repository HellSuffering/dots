﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchTimer : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    private void Awake()
    {
        enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Increment timer with delta time
    /// </summary>
    private void UpdateTimer()
    {
        m_ElapsedTime += Time.deltaTime;
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Elapsed time in seconds
    /// </summary>
    public float ElapsedTime
    {
        get { return m_ElapsedTime; }
    }

    #endregion

    #region Private Members

    // Match duration
    private float m_MatchDuration;

    // Elapsed time in seconds
    private float m_ElapsedTime;

    #endregion
}
