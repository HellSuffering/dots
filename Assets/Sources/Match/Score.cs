﻿using UnityEngine;

public class Score : MonoBehaviour
{
    #region Public Methods

    /// <summary>
    /// Increment score
    /// </summary>
    public void IncrementScore()
    {
        m_Score++;

        if (m_OnScoreChange != null)
            m_OnScoreChange.Invoke(m_Score);
    }

    #endregion

    #region Private Members

    // Invoked when score change
    [SerializeField]
    private EventInt m_OnScoreChange;

    // Score
    private int m_Score = 0;

    #endregion
}
