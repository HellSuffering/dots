﻿using UnityEngine;
using UnityEngine.Events;

public class MatchManager : MonoBehaviour
{
    #region MonoBehaviour Methods

    // Called at creation
    private void Awake()
    {
        if (s_Instance != null)
        {
            Debug.LogWarning("Multiple instances of match manager found");
            Destroy(this);
            return;
        }

        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadMap();
        CreatePlayerCameraEntity();

        m_MatchStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_MatchStarted)
            UpdateElapsedTime();
    }

    // Called on destroy
    private void OnDestroy()
    {
        s_Instance = null;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Called on match ended (timer or player death)
    /// </summary>
    public void StopMatch()
    {
        m_MatchStarted = false;

        if (m_OnMatchEnd != null)
            m_OnMatchEnd.Invoke();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load map from file and create map blocks
    /// </summary>
    private void LoadMap()
    {
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        string mapPath = Application.dataPath + "\\" + Globals.MapFolder + "\\" + m_CurrentMapName;
        EBlockID[,,] blockIDArr = MapLoader.LoadBlocks(mapPath);

        m_BlockGrid.InitializeBlockGrid(blockIDArr.GetLength(0), blockIDArr.GetLength(1), blockIDArr.GetLength(2));

        for (int x = 0; x < blockIDArr.GetLength(0); x++)
        {
            for (int y = 0; y < blockIDArr.GetLength(1); y++)
            {
                for (int z = 0; z < blockIDArr.GetLength(2); z++)
                {
                    m_BlockGrid.SetBlockAt(x, y, z, blockIDArr[x, y, z]);
                }
            }
        }
    }

    /// <summary>
    /// Create player camera entity
    /// </summary>
    private void CreatePlayerCameraEntity()
    {
        CameraFactory cameraFactory = GameSettings.Instance.CameraFactory;
        cameraFactory.CreatePlayerCameraEntity();
    }

    /// <summary>
    /// Update match elapsed time
    /// </summary>
    private void UpdateElapsedTime()
    {
        m_ElapsedTime += Time.deltaTime;

        if (m_ElapsedTime > m_MatchDuration)
        {
            m_ElapsedTime = m_MatchDuration;

            StopMatch();
        }
    }

    #endregion

    #region Getters & Setters

    /// <summary>
    /// Get match duration
    /// </summary>
    public float MatchDuration
    {
        get { return m_MatchDuration; }
    }

    /// <summary>
    /// Get elapsed time
    /// </summary>
    public float ElapsedTime
    {
        get { return m_ElapsedTime; }
    }

    /// <summary>
    /// On match end
    /// </summary>
    public UnityEvent OnMathEnd
    {
        get { return m_OnMatchEnd; }
        set { m_OnMatchEnd = value; }
    }

    /// <summary>
    /// Is match started
    /// </summary>
    public bool MatchStarted
    {
        get { return m_MatchStarted; }
    }

    /// <summary>
    /// Instance of this class
    /// </summary>
    public static MatchManager Instance
    {
        get { return s_Instance; }
    }

    #endregion

    #region Private Members

    // Name of current map
    [SerializeField]
    private string m_CurrentMapName = "";

    // Map grid
    [SerializeField]
    private BlockGrid m_BlockGrid = null;

    // Match duration in seconds
    [SerializeField]
    private float m_MatchDuration = 120;

    // Event invoked when match ends
    [SerializeField]
    private UnityEvent m_OnMatchEnd;

    // Elapsed time
    private float m_ElapsedTime;

    // Is match started
    private bool m_MatchStarted = false;

    // Instance of this class
    private static MatchManager s_Instance = null;

    #endregion
}
