﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapLoader
{
    #region Public Methods

    /// <summary>
    /// Load map file and return and array of block ID
    /// </summary>
    /// <param name="_MapFileName">Nam of map file to load</param>
    /// <returns>Array of loaded block IDs</returns>
    public static EBlockID[,,] LoadBlocks(string _MapFileName)
    {
        EBlockID[,,] blockIDArr = null;

        string strData = System.IO.File.ReadAllText(_MapFileName);

        if (strData == "")
            return null;

        string[] splitData = strData.Split(';');
        int sizeX = int.Parse(splitData[0]);
        int sizeY = int.Parse(splitData[1]);
        int sizeZ = int.Parse(splitData[2]);
        blockIDArr = new EBlockID[sizeX, sizeY, sizeZ];
        BlockSettings blockSettings = GameSettings.Instance.BlockSettings;
        EBlockID blockID;
        int index = 3;

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                for (int z = 0; z < sizeZ; z++)
                {
                    blockID = (EBlockID)(int.Parse(splitData[index]));
                    blockIDArr[x, y, z] = blockSettings.GetBlockData(blockID).BlockID;

                    index++;
                }
            }
        }

        return blockIDArr;
    }

    #endregion
}
